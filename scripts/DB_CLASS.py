"""
Project 1 - WebScraping/Plotting
This module communicates with the MYSQL Database.
Davit Voskerchyan - 2034100
"""
import mysql.connector as msc


class DB_CLASS:
    def __init__(self, host, user, passwd, db_name):
        self.host = host
        self.user = user
        self.passwd = passwd
        self.db_name = db_name
        self.connect = self.__connect()
        self.cursor = self.connect.cursor()
        # Create DB if name is provided and use it
        try:
            self.__create_database(self.db_name)
            self.__select_database(self.db_name)
        except msc.Error as err:
            print(f'Could not create/select the database: {err}')

    def populate_table(self, table_name, data):
        """
        This function populates a table.
        :param table_name: table name to be populated
        :param data: data for the populating
        """
        try:
            for record in data:
                placeholders = ",".join(['%s'] * len(record))
                query_data_to_insert = "INSERT INTO {0} VALUES({1})"
                x = (query_data_to_insert.format(table_name, placeholders))
                self.cursor.execute(x, tuple(record))
                self.connect.commit()
        except msc.Error as err:
            print(f"ERROR on data: {record}")
            print(f'Could not insert data in {table_name}: {err}')

    def get_query_data(self, query):
        """
        This function executes a query and gets the result.
        :param query: query to be executed.
        :return: results of the query.
        """
        self.cursor.execute(query)
        results = self.cursor.fetchall()
        return results

    # Create a table with its columns
    def create_table(self, table_name, table_schema):
        """
        This function creates a table.
        :param table_name: Table name of the table to be created.
        :param table_schema: Table Schema of the table to be created.
        """
        try:
            self.cursor.execute(f"DROP TABLE IF EXISTS {table_name} CASCADE")
            self.cursor.execute(f"CREATE TABLE {table_name} \
                                {table_schema} \
                                ;")
        except msc.Error as err:
            print(f'Could not connect to the DB: {err}')

    def __connect(self):
        """
        This function creates a connection to a MYSQL Database.
        :return: Database connection.
        """
        try:
            return msc.connect(
                host=self.host,
                user=self.user,
                password=self.passwd,
            )
        except msc.Error as err:
            print('Could not connect to the DB: {}'.format(err))

    def __create_database(self, db_name):
        """
        This function creates a database.
        :param db_name: Database name to be created.
        """
        try:
            self.cursor.execute(f"CREATE DATABASE IF NOT EXISTS {db_name}")
        except msc.Error as err:
            print(f"Couldn't not create database: {err}")

    def __select_database(self, db_name):
        """
        This function selects a database to be used.
        :param db_name: Database to be selected.
        """
        try:
            self.cursor.execute(f"USE {db_name}")
        except msc.Error as err:
            print(f"Could not select database: {err}")

    def close_database(self):
        """
        This function closes any connection to the database.
        """
        self.cursor.close()
        self.connect.close()
