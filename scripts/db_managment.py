"""
Project 1 - WebScraping/Plotting
This module manages the Database connection.
Database Communication passes through here.
Davit Voskerchyan - 2034100
"""
from DB_CLASS import DB_CLASS
from settings import settings


class DB_Manager:
    corona_table_day_count = 1

    def __init__(self):
        self.__db_obj = None
        self.__country_neigh_json = None
        pass

    # Creates the database object.
    def establish_connection(self):
        """
        This function creates a DB_CLASS module object, by using information in settings module.
        """
        self.__db_obj = DB_CLASS(settings.host, settings.user, settings.passwd, settings.db_name)

    def __create_corona_table(self, table_name):
        """
        This function creates a corona table.
        :param table_name: name of table.
        """
        table_schema = """(date Date,
                country VARCHAR(30) primary key,
                total_cases INT UNSIGNED default null,
                new_cases INT UNSIGNED default null,
                total_deaths INT UNSIGNED default null,
                new_deaths INT UNSIGNED default null,
                total_recovered INT UNSIGNED default null,
                new_recovered INT UNSIGNED default null,
                active_cases INT UNSIGNED default null,
                serious_critical INT UNSIGNED default null,
                total_cases_1million_population INT UNSIGNED default null,
                death_cases_1million_population INT UNSIGNED default null,
                total_tests INT UNSIGNED default null,
                tests_1million_population INT UNSIGNED default null,
                population INT UNSIGNED default null)
                """
        self.__db_obj.create_table(table_name, table_schema)

    def create_country_borders_table(self):
        """
        This function creates a country borders table.
        """
        table_name = 'country_borders_table'
        table_schema = """(id INT NOT NULL AUTO_INCREMENT,
                        country VARCHAR(100),
                        neighbour_country VARCHAR(100),
                        border DECIMAL(8,2),
                        PRIMARY KEY (id))"""
        self.__db_obj.create_table(table_name, table_schema)

    def insert_data(self, table_name, data):
        """
        This function inserts data into a table.
        :param table_name: table name of the table.
        :param data: data to be inserted.
        """
        self.__db_obj.populate_table(table_name, data)

    def populate_country_borders_table(self):
        """
        This function populates the country borders' table with the json file.
        """
        import json
        json_file_path = "../countries_json/country_neighbour_dist_file.json"
        with open(json_file_path, 'r') as j:
            json_contents = json.loads(j.read())
        # Loop through each dictionary
        country_id = 1
        for dic in json_contents:
            country = list(dic.keys())[0]
            values = dic.values()
            # Getting the value of the dictionary
            all_neighbouring_countries_dict = list(values)[0]
            # Getting neighbouring countries
            neighbour_countries = all_neighbouring_countries_dict
            for neighbour in neighbour_countries:
                border_length = neighbour_countries.get(neighbour)
                self.insert_data("country_borders_table", [[country_id, country, neighbour, border_length]])
                country_id += 1

    def populate_corona_table(self, table_name, data, date):
        """
        This function populates a corona table.
        :param table_name: table name of the table.
        :param data: data to be inserted
        :param date: date when data is gotten.
        """
        table_name = f"corona_table_{table_name}"
        self.__create_corona_table(table_name)
        for record in data:
            record = record[1:15]  # Slices only the data needed for table
            record.insert(0, date)
            self.insert_data(table_name, [record])

    def get_neighbour_with_longest_border(self, country):
        """
        This function gets the neighbour of a country who share the longest border.
        :param country: name of the country.
        :return: name of the neighbour country.
        """
        from operator import itemgetter
        query = f"select neighbour_country,border from country_borders_table where country='{country}';"
        results = self.__db_obj.get_query_data(query)
        try:
            res = max(results, key=itemgetter(1))
        except ValueError:
            print(f"This country {country} has no neighbours")
            return None
        # Country has a Neighbour
        return res[0]

    def get_3_neighbours(self, country):
        """
        This function gets the 3 neighbours with the longest sharing border.
        :param country: name of country.
        :return: names of 3 neighbours in a list.
        """
        from operator import itemgetter
        query = f"select neighbour_country,border from country_borders_table where country='{country}';"
        results = self.__db_obj.get_query_data(query)
        res = sorted(results, key=itemgetter(1))
        return res[len(res) - 3:len(res)]

    def close_database_connection(self):
        """
        This function closes the connection to the database.
        """
        self.__db_obj.close_database()

    def get_db_obj(self):
        """
        This function gets the DB_CLASS object.
        :return: DB_CLASS object.
        """
        return self.__db_obj
