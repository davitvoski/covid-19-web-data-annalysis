"""
Project 1 - WebScraping/Plotting
This module plots the analyzed data.
Davit Voskerchyan - 2034100
"""
from matplotlib import pyplot as plt


class Data_Plotting:

    def __init__(self, country, df_country, neigh_longest_border=None, df_neigh_longest_border=None, title=None,
                 neigh1=None, neigh2=None, neigh3=None, df_neigh1=None, df_neigh2=None, df_neigh3=None,
                 legend=None, xlabel=None, ylabel=None):
        self.__country = country
        self.__neigh_longest_border = neigh_longest_border
        self.__neigh1 = neigh1
        self.__neigh2 = neigh2
        self.__neigh3 = neigh3
        self.__title = title
        self.__legend = legend
        self.__df_country = df_country
        self.__df_neigh_longest_border = df_neigh_longest_border
        self.__df_neigh1 = df_neigh1
        self.__df_neigh2 = df_neigh2
        self.__df_neigh3 = df_neigh3
        # # Check for empty dataframes
        # if self.__df_neigh1.empty:
        #     self.__df_neigh1 = None
        #     self.__neigh1 = None
        # if self.__df_neigh2.empty:
        #     self.__df_neigh2 = None
        #     self.__neigh2 = None
        # if self.__df_neigh3.empty:
        #     self.__df_neigh3 = None
        #     self.__neigh3 = None
        self.__xlabel = xlabel
        self.__ylabel = ylabel

    def histogram_plot_one_dataframe_new_cases_deaths_recovered_7(self):
        """
        Plots a histogram plot for a dataframes, Data for #7
        Using new cases, new deaths, new recovered keys
        """
        try:
            # Plotting
            fig, axes = plt.subplots(3)
            axes[0].hist(self.__df_country.new_cases.dropna(), bins=range(0, 500000, 25000),
                         label='new_cases', alpha=0.75)
            axes[1].hist(self.__df_country.new_deaths.dropna(), bins=range(0, 1000, 50),
                         label='new_deaths', alpha=0.55)
            axes[2].hist(self.__df_country.new_recovered.dropna(), bins=range(0, 500000, 30000),
                         label='new_recovered', alpha=0.25)

            # Attributes
            fig.legend(self.__legend)
            fig.suptitle(self.__title)
            fig.supxlabel(self.__xlabel)
            fig.supylabel(self.__ylabel)
            # screen for TkAgg backend
            if plt.get_backend() == 'TkAgg':
                mng = plt.get_current_fig_manager()
                mng.window.state('zoomed')
            fig.show()
        except ValueError as e:
            print("Couldn't plot", e)

    def scatter_plot_two_dataframes_number_8(self):
        """
         Plots a scatter plot for two dataframes, Data for #8.
         Using new cases against neighbour country with the longest border
        """
        del self.__df_country['country']
        del self.__df_neigh_longest_border['country']
        # Plotting
        try:
            plt.scatter(self.__df_country.date, self.__df_country.new_cases)
            plt.scatter(self.__df_neigh_longest_border.date, self.__df_neigh_longest_border.new_cases)
            # Attributes
            plt.legend(self.__legend)
            plt.title(self.__title)
            plt.xlabel(self.__xlabel)
            plt.ylabel(self.__ylabel)
            # screen for TkAgg backend
            if plt.get_backend() == 'TkAgg':
                mng = plt.get_current_fig_manager()
                mng.window.state('zoomed')
            plt.show()
        except ValueError as e:
            print("Couldn't plot", e)

    def histogram_plot_country_3neighbours_death_cases_per_1milpop_9(self):
        """
         Plots a histogram plot for 4 dataframes, Data for #9.
         Using death cases per 1 million population of a country
         Against it's 3 or less neighbour countries.
        """
        # Plotting
        try:
            plt.hist(self.__df_country.death_cases_1million_population.dropna(),
                     bins=range(0, 10000, 1000), label=f'{self.__country} death cases/1mil')
            if (self.__df_neigh1 is not None) and (not self.__df_neigh1.empty):
                plt.hist(self.__df_neigh1.death_cases_1million_population.dropna(),
                         bins=range(0, 10000, 1000), label=f'{self.__neigh1} death cases/1mil')
            if (self.__df_neigh2 is not None) and (not self.__df_neigh2.empty):
                plt.hist(self.__df_neigh2.death_cases_1million_population.dropna(),
                         bins=range(0, 10000, 1000), label=f'{self.__neigh2} death cases/1mil')
            if (self.__df_neigh3 is not None) and (not self.__df_neigh3.empty):
                plt.hist(self.__df_neigh3.death_cases_1million_population.dropna(),
                         bins=range(0, 10000, 1000), label=f'{self.__neigh3} death cases/1mil')

            # Attributes
            plt.title(self.__title)
            # Clear None Neighbour Names Since No Neighbours
            plt.legend([x for x in self.__legend if x != 'None'])
            plt.xlabel(self.__xlabel)
            plt.ylabel(self.__ylabel)
            # screen for TkAgg backend
            if plt.get_backend() == 'TkAgg':
                mng = plt.get_current_fig_manager()
                mng.window.state('zoomed')
            plt.show()
        except ValueError as e:
            print("Couldn't plot", e)
