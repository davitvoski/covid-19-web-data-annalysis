"""
Project 1 - WebScraping/Plotting
This module is the main program where all
communication happens with the user
Davit Voskerchyan - 2034100
"""
from scraping import Web_Scraping_Class
from db_managment import DB_Manager
from data_cleaner import CLEANER_CLASS as Cleaner
from data_analyse import Data_Analyze
import os


class Main_Program:

    def __init__(self):
        self.__scraped_data = None
        self.__clean_data = []
        self.__day = None
        self.__manager = None

    def __save_web_to_local(self, url):
        """
        This function saves a website html locally.
        :param url: URL of the website ex: https://www.worldometers.info/coronavirus/
        """
        scrape = Web_Scraping_Class(url)
        scrape.request_data()
        scrape.local_save()
        print("Finished Saving website to local .html file.")

    def __scrape_from_local_html(self, abs_path):
        """
        This function scrapes from a local html page.
        :param abs_path: Absolute path of the location of the page.
        """
        print(f"Starting process for {abs_path} file.")
        scrape = Web_Scraping_Class(abs_path)
        scrape.request_data()
        # scrape.read_local_html_file_binary()
        scrape.scrape_data()
        self.__scraped_data = scrape.get_data()
        print(f"Finished Scraping local html {abs_path} file.")
        # Calls data cleaner
        self.__clean_scraped_data()

    def __clean_scraped_data(self):
        """
        This function cleans the scraped data.
        """
        print("Started Cleaning data.")
        for i in range(0, 3):
            cleaner = Cleaner(self.__scraped_data[i])
            cleaned = cleaner.get_clean_data()
            self.__clean_data.append(cleaned)
        print("Finished Cleaning data.")
        # Calls database manager to save data

    def __save_scraped_data_to_database(self):
        """
        This function creates the necessary tables
        to save the scraped clean data in DB.
        """
        print("Saving clean data to database.")
        self.__manager = DB_Manager()
        self.__manager.establish_connection()
        # Creating table and populating from json file
        self.__manager.create_country_borders_table()
        self.__manager.populate_country_borders_table()
        # Create and Populate corona table
        for i in range(6, 0, -1):
            # Checks if what day needs to be.
            self.__manager.populate_corona_table(f"day{str(i)}",
                                                 self.__clean_data[i - 1],
                                                 f'2022-03-{self.__day}')
            if int(self.__day) - i <= 0:
                self.__day = str(31 - (i - int(self.__day)))
            else:
                self.__day = str(int(self.__day) - 1)
        print("Finished saving clean data to database.")

    def __plotting_ui(self):
        """
        This function Analyzes and Plots the graphs per the
        inputted country.
        """
        country = input("\nWhich Country you would like to analyze: ")
        analyze = Data_Analyze(country)
        analyze.analyze_evolution_6days_new_cases_deaths_recovered()
        analyze.analyze_evolution_6days_new_cases_with_longest_border_neigh(
            self.__manager.get_neighbour_with_longest_border(country)
        )
        analyze.analyze_evolution_3days_cases_of_death_per_1mil_pop(
            self.__manager.get_3_neighbours(country)
        )

    def main(self):
        """
        This function is the main function.
        Entry point of the program.
        """
        main_menu = True
        while main_menu:
            print("1. Save to local web file.")
            print("2. Scrape from local file.")
            print("3. To Exit.")
            choice = input("Enter here: ")
            if choice == '1':
                url = 'https://www.worldometers.info/coronavirus/'
                self.__save_web_to_local(url)
            if choice == '2':
                self.__day = input("Enter the day of the file you want to scrape: ")
                # Validating day
                if int(self.__day) < 1 or int(self.__day) > 31:
                    print("Enter a valid day. (1-31)")
                    break
                # First html file
                abs_path_1 = os.path.abspath(
                    f"../local_html/local_page2022-3-{self.__day}.html")
                url_path_1 = f'file:///{abs_path_1}'
                # Second html file, 3 days later, check for above 31 back to 1
                if int(self.__day) + 3 >= 31:
                    self.__day = str((int(self.__day) + 3) - 31)
                else:
                    self.__day = str(int(self.__day) + 3)

                abs_path_2 = os.path.abspath(
                    f"../local_html/local_page2022-3-{self.__day}.html")
                url_path_2 = f'file:///{abs_path_2}'
                # Scrapping
                self.__scrape_from_local_html(url_path_1)
                self.__scrape_from_local_html(url_path_2)
                # Saving to database
                print("Started Database process.")
                self.__save_scraped_data_to_database()
                print("Finished Database process.")
                # Calling Plotting UI
                plotting_menu = True
                while plotting_menu:
                    self.__plotting_ui()
                    choice = input("Would you like to exit? (y/n):")
                    if choice == "y":
                        self.__manager.close_database_connection()
                        plotting_menu = False
                        main_menu = False
            # Exit choice
            if choice == "3": main_menu = False
