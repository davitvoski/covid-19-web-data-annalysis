"""
Project 1 - WebScraping/Plotting
This module analyses the data from the database.
Davit Voskerchyan - 2034100
"""
import pandas as pd
import sqlalchemy
from data_visualization import Data_Plotting
from settings import settings


class Data_Analyze:

    def __init__(self, country):
        self.__country = country
        try:
            # Leave the engine as a one liner so no problems are caused
            self.__engine = sqlalchemy.create_engine(
                f"""mysql+mysqlconnector://{settings.user}:{settings.passwd}@{settings.host}/{settings.db_name}?charset=utf8mb4""")
        except sqlalchemy.exc.ProgrammingError as err:
            print(f"Failed  to create an engine: {err}")

    def analyze_evolution_6days_new_cases_deaths_recovered(self):
        """
         This function analyses the evolution for 6 days of a country
         for three indicators: new_cases, new_deaths, new_recovered
        """
        # Create Query
        columns = "date,new_cases,new_deaths,new_recovered"
        table = "corona_table_day"
        condition = f"country = '{self.__country}'"
        query_day1 = f"select {columns} from {table}1 where {condition}"
        query_day2 = f"select {columns} from {table}2 where {condition}"
        query_day3 = f"select {columns} from {table}3 where {condition}"
        query_day4 = f"select {columns} from {table}4 where {condition}"
        query_day5 = f"select {columns} from {table}5 where {condition}"
        query_day6 = f"select {columns} from {table}6 where {condition}"
        query = f"""{query_day1} union {query_day2} union {query_day3} union 
                    {query_day4} union {query_day5} union {query_day6};"""
        # Preparing dataframe
        my_df = pd.read_sql_query(query, self.__engine)
        # Attributes
        title = f"6 days Covid Evolution of New Cases,Deaths,Recovered for {self.__country.upper()}"
        legend = ['New Cases', 'New Deaths', 'New Recovered']
        xlabel = 'Number Of Cases'
        ylabel = 'Number Of Occurrences'
        # Plotting
        plotting = Data_Plotting(country=self.__country, title=title, legend=legend,
                                 xlabel=xlabel, ylabel=ylabel, df_country=my_df)
        plotting.histogram_plot_one_dataframe_new_cases_deaths_recovered_7()

    def analyze_evolution_6days_new_cases_with_longest_border_neigh(self, neighbour):
        """
         This function analyzes the evolution for 6 days for a country
         with its longest bordered neighbour on indicator: New Cases
        :param neighbour: Name of the neighboured country.
        """
        columns = 'date,country,new_cases'
        table = 'corona_table_day'
        df_country = None
        df_neighbour = None
        count = 0
        # Creating query and getting dataframes
        while True:
            condition = f'country = "{self.__country}"'
            if count == 1:
                condition = f'country = "{neighbour}"'
            query_day1 = f"select {columns} from {table}1 where {condition}"
            query_day2 = f"select {columns} from {table}2 where {condition}"
            query_day3 = f"select {columns} from {table}3 where {condition}"
            query_day4 = f"select {columns} from {table}4 where {condition}"
            query_day5 = f"select {columns} from {table}5 where {condition}"
            query_day6 = f"select {columns} from {table}6 where {condition}"
            query = f"""{query_day1} union {query_day2} union {query_day3} union 
                        {query_day4} union {query_day5} union {query_day6};"""
            if count == 1:
                df_neighbour = pd.read_sql_query(query, self.__engine)
                break
            else:
                df_country = pd.read_sql_query(query, self.__engine)
                count += 1
        # Plotting
        title = f"6 Days: New Cases of Covid in {self.__country} vs {neighbour}"
        legend = ''
        if neighbour is not None:
            legend = [f'{self.__country.upper()} New Cases', f'{neighbour.upper()} New Cases']
        else:
            legend = [f'{self.__country.upper()} New Cases', 'No Neighbour']
        xlabel = "Date"
        ylabel = "Number Of New Cases"
        plotting = Data_Plotting(country=self.__country, neigh_longest_border=neighbour,
                                 title=title, legend=legend, xlabel=xlabel, ylabel=ylabel,
                                 df_country=df_country, df_neigh_longest_border=df_neighbour)

        plotting.scatter_plot_two_dataframes_number_8()

    def analyze_evolution_3days_cases_of_death_per_1mil_pop(self, neighbours):
        """
         This function analyzes the evolution for 3 days for a country
         with its 3 closest neighbours on
         indicator: date,country,death_cases_1million_population
        :param neighbours: List of the names of all three neighbours.
        """
        # Getting names of neighboured countries.
        neigh1 = None
        neigh2 = None
        neigh3 = None
        # less than 3 neighbours or no neighbours check
        if len(neighbours) == 3:
            neigh1 = neighbours[0][0]
            neigh2 = neighbours[1][0]
            neigh3 = neighbours[2][0]
        elif len(neighbours) == 2:
            neigh1 = neighbours[0][0]
            neigh2 = neighbours[1][0]
        elif len(neighbours) == 1:
            neigh1 = neighbours[0][0]

        df_country = None
        df_neigh1 = None
        df_neigh2 = None
        df_neigh3 = None
        # Creating query and getting dataframes
        columns = "date,country,death_cases_1million_population"
        table = "corona_table_day"
        condition = f"country = '{self.__country}'"
        count = 0
        while True:
            if count == 1 and neigh1 is not None: condition = f'country = "{neigh1}"'
            if count == 2 and neigh2 is not None: condition = f'country = "{neigh2}"'
            if count == 3 and neigh3 is not None: condition = f'country = "{neigh3}"'
            query_day1 = f"select {columns} from {table}1 where {condition}"
            query_day2 = f"select {columns} from {table}2 where {condition}"
            query_day3 = f"select {columns} from {table}3 where {condition}"
            query = f"{query_day1} union {query_day2} union {query_day3};"
            # Getting dataframes
            if count == 0: df_country = pd.read_sql_query(query, self.__engine)
            if count == 1 and neigh1 is not None: df_neigh1 = pd.read_sql_query(query, self.__engine)
            if count == 2 and neigh2 is not None: df_neigh2 = pd.read_sql_query(query, self.__engine)
            if count == 3 and neigh3 is not None:
                df_neigh3 = pd.read_sql_query(query, self.__engine)
                break
            # Break after analyzing dataframe for country if no neighbours exist.
            if neigh1 is None and neigh2 is None and neigh3 is None: break
            # Break condition in whatever case
            if count == 3: break
            count += 1
        # Attributes
        title = f'3 days: Death Cases Per 1 Million Population of {self.__country} vs 3 Closest Neighbours'
        xlabel = 'Number Of Cases'
        ylabel = 'Number Of Occurrences'
        legend = [f"{self.__country}", f"{neigh1}", f"{neigh2}", f"{neigh3}"]
        plotting = Data_Plotting(country=self.__country, df_country=df_country, df_neigh1=df_neigh1,
                                 df_neigh2=df_neigh2, df_neigh3=df_neigh3, title=title,
                                 xlabel=xlabel, ylabel=ylabel, legend=legend)
        plotting.histogram_plot_country_3neighbours_death_cases_per_1milpop_9()
