"""
Project 1 - WebScraping/Plotting
This module cleans the data scraped.
Davit Voskerchyan - 2034100
"""


class CLEANER_CLASS:

    def __init__(self, data):
        self.__data = data
        self.__clean_data = []
        self.__cleaning_data()

    def __cleaning_data(self):
        """
          This function cleans the data given.
        """
        for record in self.__data:
            clean_record = []
            for elem in record:
                if elem == '\n': continue
                elem = elem.strip()
                elem = elem.replace("\n", "")
                elem = elem.replace(',', "")
                elem = elem.replace("+", "")
                elem = elem.replace("-", "")
                elem = elem.strip()
                # empty string means no value/empty box on website
                if elem == '': elem = None
                # cast to float
                elem = self.__cast_to_int(elem)
                # append clean record
                clean_record.append(elem)
            self.__clean_data.append(clean_record)

    def __cast_to_int(self, elem):
        """
        This function casts an element to int when cleaning.
        :param elem: Element to be cast
        :return: cast element or None
        """
        if elem is None: return elem
        if elem == "N/A": return None
        """
        Since everything are whole numbers, we convert to int 
        Only 1 field, country is a string hence the check of country.isalpha() 
        """
        country = elem.replace(" ", "").replace(".", "").replace("/", "").replace("-", "")
        if country.isalpha(): return elem
        # Since some data that isn't being used are float numbers
        # first cast to float than int.
        return int(float(elem))

    def get_clean_data(self):
        """
        This function gets the clean data.
        :return: the cleaned data.
        """
        return self.__clean_data
