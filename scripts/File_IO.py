"""
Project 1 - WebScraping/Plotting
This module writes and read data from/to a file.
Davit Voskerchyan - 2034100
"""


class File_IO:

    def read_country_neighbour_json(self):
        """
        This function reads the data from the countries_json/country_neighbour_dist_file.json .
        :return: data gathered from the file.
        """
        try:
            import json
            # import pprint 
            json_file_path = "countries_json/country_neighbour_dist_file.json"
            with open(json_file_path, 'r') as j:
                return json.loads(j.read())
            # pprint.pp(json_contents)
        except json.JSONDecodeError as err:
            print(f"Could not read from file {json_file_path}: {err}")

    def write_data(self, path, html_binary):
        """
        This function writes data to a file.
        :param path: path of the file to be written.
        :param html_binary: data to be written.
        """
        try:
            file_obj = open(path, 'w+b')
            file_obj.write(html_binary)
            file_obj.close()
        except IOError as err:
            print(f"Could not write to file {path}: {err}")
