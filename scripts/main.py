"""
Project 1 - WebScraping/Plotting
This module is where the Application starts.
Run this module to run the Application.
Davit Voskerchyan - 2034100
"""
from main_program import Main_Program

if __name__ == "__main__":
    main = Main_Program()
    main.main()
