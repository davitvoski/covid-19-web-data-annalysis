"""
Project 1 - WebScraping/Plotting
This module is for saving data and web scraping.
Davit Voskerchyan - 2034100
"""
from urllib.request import urlopen, Request
from urllib import error
from bs4 import BeautifulSoup as bs4


class Web_Scraping_Class:

    def __init__(self, url):
        self.__url = url
        self.__site_html_binary = None
        self.__root_obj = None
        self.__data = []

    def request_data(self):
        """
        This function requests the data from an html page.
        """
        try:
            req = Request(self.__url, headers={"user-agent": "Mozilla/5.0"})
            html_resp = urlopen(req)
            self.__site_html_binary = html_resp.read()
        except error.URLError as err:
            print(f"Could not request data: {err}")

    def local_save(self):
        """
        This function saves the html page locally to local_html directory.
        """
        from datetime import datetime
        from File_IO import File_IO
        file_name = f"local_page2022-{datetime.today().month}-{datetime.today().day}.html"
        path = f"../local_html/{file_name}"
        File_IO().write_data(path, self.__site_html_binary)

    def scrape_data(self):
        """
        This function scrapes data from a html page.
        """
        try:
            self.__root_obj = bs4(self.__site_html_binary, features="html.parser")
            # Gets all tables, now, yesterday, yesterday2
            all_tables = self.__root_obj.findAll('table')
            for table in all_tables:
                # Data located at 9-235.
                trs = table.findAll('tr')[9:236]
                table_data = []
                for record in trs:
                    table_data.append([d.text for d in record])
                self.__data.append(table_data)
        except AttributeError as err:
            print(f"Error when scarping: {err}")

    def get_data(self):
        """
        This function gets the data obtained from scraping.
        :return: scraped data.
        """
        return self.__data
