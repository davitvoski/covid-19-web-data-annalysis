# 420 Python Project1

## Introduction

This project was done in my class of Web Development - server-side programming using __PYTHON__. <br>
This project demonstrates our skills of scraping a website with Covid-19 data and plotting some graphs while using OOP. <br>
This is the [website ](https://www.worldometers.info/coronavirus/). 

## Description
This project uses several modules that separates each part of the functionality in it's own module. 
In this project, we scraped data from a website for 6 days meaning we save the page locally every 3 days to have a full dataset

Afterwards, we clean the scraped data and save it to a __MYSQL__ Database. Also we create a table and save the data of the json file located in _country_json_ directory which shows the neighbours of a country and the length of the border they share.

Once all data is saved, we analyze the data for three cases using __pandas__. 

The first case is we analyze the evolution through 6 days of a country given by the user on three __key indicators__ : _NewCases_ , _NewDeaths_ , _NewRecovered_.

The second case is we analyze the evolution through 6 days of a country given by the user and compare it with the neighbour that shares the longest border with the country given on the __key indicator__ : _NewCases_. 

The third case is we analyze the difference through 3 days of a country given by the user and compare it to at most 3 of its neighbours, they are chosen based on how long the shared border is on the  __key indicator__ : _deaths per milion population_.

After each case is analyzed the data will be put in a __pandas dataframe__ and than using __matplotlib__ we will plot the dataframes according to each of their respective plottings.  
First and third cases are plotted as a Histogram plot.
Second case is plotted using a Scatter plot. <br>

***For Description and Examples, visit the report.docx document.***

## Requirements

For the following project to work please install:<br>
&emsp;    1. Install Python 3.10. https://www.python.org/downloads/ (3.9 should work aswell)<br>
&emsp;    2. Install the following python packages with pip command at terminal:<br>
&emsp;&emsp;&emsp;        -   pip install sqlalchemy<br>
&emsp;&emsp;&emsp;        -   pip install matplotlib<br>
&emsp;&emsp;&emsp;        -   pip install pandas<br>
&emsp;&emsp;&emsp;        -   pip install mysql-connector-python<br>
 &emsp;&emsp;&emsp;       -   pip install bs4<br>

### Example 
[See Example](./misc/example.png)

## Start up

To Start the project, open the project in your desired _IDE_. <br> 
In the scripts folder open main.py file. <br>
Run the main.py file and enjoy.

### Sample run
[Step 1](./misc/Sample-Run-First.png) <br>
[Step 2](./misc/Sample-Run-Second.png)<br>
[Step 3](./misc/Sample-Run-Third.png)<br>
[Result Case Three](./misc/Sample-Run-Fourth.png)<br>

## Authors and acknowledgment
<h3> Davit Voskerchyan </h3>
- [Profile](https://gitlab.com/Davit_V "Davit Voskerchyan")
- [Email](mailto:davitvoskerchyan@gmail.com?subject=4th semester Python Project1 "4th semester Python Project1")

## License
[Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)
